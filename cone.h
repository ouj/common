#ifndef _CONE_H_
#define _CONE_H_

#include "stdmath.h"
#include "debug.h"
#include "range.h"

template<typename R>
struct cone3 {
    cone3() : dirbound(invalidrange3<R>()) {} 
    cone3(const vec3<R> &v) : dirbound(v, v) {}
    vec3<R> getAxis() const { return normalize(dirbound.center()); }
    range3<R> dirbound;
};

template <typename R>
R cosAngle(const cone3<R> &c) {
    vec3<R> center = c.dirbound.center();
    R r2 = c.dirbound.diagonalSqr() * ((R)0.25);
    R d2 = center.lengthSqr();
    if(d2 == (R)0) {
        return (R)0;
    }
    R d = sqrt(d2);
    R a = (d2 - r2 + 1) / (2 * d);
    return clamp(a, (R)0, (R)1);
}

template <typename R>
R sinAngle(const cone3<R> &c) {
    R a = getCosAngle(c);
    return clamp(sqrt(1 - c*c), (R)0, (R)1);
}

template <typename R>
bool isvalid(const cone3<R> &c) {
    vec3<R> center = center(c.dirbound);
    R r2 = lengthSqr(size(c.dirbound)) * ((R)0.25);
    R d2 = lengthSqr(center);
    if(d2 == (R)0) {
        return false;
    }
    R d = sqrt(d2);
    R a = (d2 - r2 + 1) / (2 * d);
    return (a >= (R)0) && (a <= (R)1);
}

template <typename R>
cone3<R> runion(const cone3<R> &c, const vec3<R> &v) {
    c.dirbound = runion(c.dirbound, v);
}

template <typename R>
cone3<R> runion(const cone3<R> &c1, const cone3<R> &c2) {
    cone3<R> c;
    c.dirbound = runion(c1.dirbound, c2.dirbound);
    return c;
}

typedef cone3<float> cone3f;
typedef cone3<double> cone3d;

#endif
