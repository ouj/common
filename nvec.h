#ifndef _NVEC_H_
#define _NVEC_H_

#include "stdmath.h"
#include <string>
#include <tuple>
#include <stdexcept>

template<int N>
struct nvec {
    nvec() {};
    nvec(std::initializer_list<float> args);
    nvec(std::initializer_list<vec3f> args);
    float operator[](int8_t i) const { return v[i]; }
    float& operator[](int8_t i) { return v[i]; }
    int8_t max_component_index() const;
    int8_t min_component_index() const;
    inline nvec& operator+=(const nvec& b) { for(int i = 0; i < N; i++) v[i] += b.v[i]; return (*this); }
    inline nvec& operator-=(const nvec& b) { for(int i = 0; i < N; i++) v[i] -= b.v[i]; return (*this); }
    inline nvec& operator*=(const nvec& b) { for(int i = 0; i < N; i++) v[i] *= b.v[i]; return (*this); }
    inline nvec& operator/=(const nvec& b) { for(int i = 0; i < N; i++) v[i] /= b.v[i]; return (*this); }
    inline nvec& operator*=(const float b) { for(int i = 0; i < N; i++) v[i] *= b; return (*this); };
    inline nvec& operator/=(const float b) { for(int i = 0; i < N; i++) v[i] /= b; return (*this); }

    friend nvec operator-(const nvec& a) { nvec b; for(int i = 0; i < N; i++) b.v[i] = -a.v[i]; return b; }
    friend nvec operator+(const nvec& a, const nvec& b) { nvec c; for(int i = 0; i < N; i++) c.v[i] = a.v[i] + b.v[i]; return c; }
    friend nvec operator-(const nvec& a, const nvec& b) { nvec c; for(int i = 0; i < N; i++) c.v[i] = a.v[i] - b.v[i]; return c; }
    friend nvec operator*(const nvec& a, const nvec& b) { nvec c; for(int i = 0; i < N; i++) c.v[i] = a.v[i] * b.v[i]; return c; }
    friend nvec operator/(const nvec& a, const nvec& b) { nvec c; for(int i = 0; i < N; i++) c.v[i] = a.v[i] / b.v[i]; return c; }

    friend nvec operator*(const nvec& a, float b) { nvec c; for(int i = 0; i < N; i++) c.v[i] = a.v[i] * b; return c; }
    friend nvec operator/(const nvec& a, float b) { nvec c; for(int i = 0; i < N; i++) c.v[i] = a.v[i] / b; return c; }

    friend nvec floor_component(const nvec& a) { nvec b; for(int i = 0; i < N; i++) b.v[i] = floor(a.v[i]); return b; }

    inline float lengthSqr() const { return dot(*this,*this); }
    inline float length() const { return sqrt(lengthSqr()); }

    friend float dot(const nvec& a, const nvec& b) { float d = 0; for(int i = 0; i < N; i++) d += a.v[i] * b.v[i]; return d; }

    float v[N];
};

template<int N>
nvec<N>::nvec(std::initializer_list<vec3f> args) {
    if (args.size() * 3 != N) throw std::invalid_argument("argument number donot match");
    else {
        int i = 0;
        for (auto it = args.begin(); it != args.end(); ++it) {
            v[i+0] = it->x;
            v[i+1] = it->y;
            v[i+2] = it->z;
            i += 3;
        }
    }
}

template<int N>
nvec<N>::nvec(std::initializer_list<float> args) {
    if (args.size() != N) throw std::invalid_argument("argument number donot match");
    else {
        int i = 0;
        for (auto it = args.begin(); it != args.end(); ++it) v[i++] = *it;
    }
}

template<int N>
int8_t nvec<N>::max_component_index() const {
    float m = v[0]; int8_t mi = 0;
    for(int i = 1; i < N; i++) if (m < v[i]) { mi = i; m = v[i]; }
    return mi;
};

template<int N>
int8_t nvec<N>::min_component_index() const {
    float m = v[0]; int8_t mi = 0;
    for(int i = 1; i < N; i++) if (m > v[i]) { mi = i; m = v[i]; }
    return mi;
};

#endif
