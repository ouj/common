#ifndef _NRANGE_H
#define _NRANGE_H

#include "stdmath.h"
#include "debug.h"
#include "nvec.h"

template<int N>
struct nrange {
    nrange() {};
    nrange(const nvec<N> &min, const nvec<N> &max) : min(min), max(max) {}
    void        grow(const nvec<N>& p);
    nvec<N>     extent() const { nvec<N> e; for(int i = 0; i < N; i++) e[i] = max[i] - min[i]; return e; }
    nvec<N>     center() const { nvec<N> e; for(int i = 0; i < N; i++) e[i] = (max[i] + min[i]) * 0.5f; return e; }

    nvec<N> min;
    nvec<N> max;
};

template<int N>
void nrange<N>::grow(const nvec<N>& p) {
    for(int i = 0; i < N; i++) {
        min[i] = std::min(min[i], p[i]);
        max[i] = std::max(max[i], p[i]);
    }
}

template<int N>
nrange<N> invalidnrange() {
    nrange<N> r;
    for(int i = 0; i < N; i++) {
        r.min[i] = numeric_limits<float>::max();
        r.max[i] = -numeric_limits<float>::max();
    }
    return r;
}

#endif
