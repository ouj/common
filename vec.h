#ifndef _VEC_H_
#define _VEC_H_

#include "stdmath.h"
#include <string>
#include <tuple>

template <typename R>
struct vec2 {
    R x, y;
    vec2() = default;
    vec2(R x, R y) : x(x), y(y) {}

    inline vec2& operator=(const std::tuple<R,R> &t) { x=std::get<0>(t); y=std::get<1>(t); return *this;}

    inline vec2& operator+=(const vec2<R>& b) { x+=b.x; y+=b.y; return (*this); }
    inline vec2& operator-=(const vec2<R>& b) { x-=b.x; y-=b.y; return (*this); }
    inline vec2& operator*=(const vec2<R>& b) { x*=b.x; y*=b.y; return (*this); }
    inline vec2& operator/=(const vec2<R>& b) { x/=b.x; y/=b.y; return (*this); }
    inline vec2& operator*=(const R b) { x*=b;y*=b; return (*this); }
    inline vec2& operator/=(const R b) { x/=b;y/=b; return (*this); }

    inline R operator[](int index) const { return ((R*)this)[index]; }
    inline R& operator[](int index) { return ((R*)this)[index]; }

    inline R lengthSqr() const { return dot(*this,*this); }
    inline R length() const { return sqrt(lengthSqr()); }
    inline int max_component_index() const { return (x>=y) ? 0 : 1; }

    std::string print() const;

    inline bool iszero() const { return x == 0 && y == 0; }
    inline bool hasnan() const { return isnan(x) || isnan(y); }
    inline bool hasinf() const { return isinf(x) || isinf(y); }
    inline bool hasneg() const { return x < 0 || y < 0; }
    inline bool hasinvalid() const { return hasinf() || hasnan() || hasneg(); }

    inline std::tuple<R,R> unpack() const { return std::tuple<R,R>(x, y); }

    friend vec2 operator-(const vec2& a) { return vec2(-a.x,-a.y); }
    friend vec2 operator+(const vec2& a, const vec2& b) { return vec2(a.x+b.x,a.y+b.y); }
    friend vec2 operator-(const vec2& a, const vec2& b) { return vec2(a.x-b.x,a.y-b.y); }
    friend vec2 operator*(const vec2& a, const vec2& b) { return vec2(a.x*b.x,a.y*b.y); }
    friend vec2 operator/(const vec2& a, const vec2& b) { return vec2(a.x/b.x,a.y/b.y); }
    friend vec2 operator*(const vec2& a, const R b) { return vec2(a.x*b,a.y*b); }
    friend vec2 operator/(const vec2& a, const R b) { return vec2(a.x/b,a.y/b); }
    friend vec2 operator*(const R a, const vec2& b) { return vec2(a*b.x,a*b.y); }
    friend vec2 operator/(const R a, const vec2& b) { return vec2(a/b.x,a/b.y); }
    friend bool operator==(const vec2& a, const vec2& b) { return a.x==b.x&&a.y==b.y&&a.z==b.z; }
    friend bool operator!=(const vec2& a, const vec2& b) { return !(a==b); }
    friend R dot(const vec2& a, const vec2& b) { return a.x*b.x+a.y*b.y; }
    friend vec2 abs_component(const vec2& a) { return vec2(abs(a.x), abs(a.y)); }
    friend vec2 exp_component(const vec2& a) { return vec2(exp(a.x), exp(a.y)); }
    friend vec2 pow_component(const vec2& a, R e) { return vec2(pow(a.x,e), pow(a.y,e)); }
    friend vec2 sqrt_component(const vec2& a) { return vec2((R)sqrt(a.x), (R)sqrt(a.y)); }
    friend vec2 min_component(const vec2& a, const vec2& b) { return vec2(min(a.x,b.x),min(a.y,b.y)); }
    friend vec2 max_component(const vec2& a, const vec2& b) { return vec2(max(a.x,b.x),max(a.y,b.y)); }
    friend vec2 floor_component(const vec2& a) { return vec2(floor(a.x), floor(a.y)); }
    friend vec2 ceil_component(const vec2& a) { return vec2(ceil(a.x), ceil(a.y)); }
    friend vec2 normalize(const vec2& a) { R l = sqrt(dot(a, a)); return (l == 0) ? a : a/l; }
};

template<typename R>
inline vec2<R> makevec2(R a) { return vec2<R>(a, a); }

template<typename R>
inline std::string vec2<R>::print() const {
    char s[64]; sprintf(s, "[%f,%f]",x,y); return std::string(s);
}
template <>
inline std::string vec2<int>::print() const {
    char s[64]; sprintf(s, "[%d,%d]",x,y); return std::string(s);
}

template <typename R>
struct vec3 {
    R x, y, z;
    vec3() = default;
    vec3(R x, R y, R z) : x(x), y(y), z(z) {}

    inline vec3& operator=(const std::tuple<R,R,R> &t) { x=std::get<0>(t); y=std::get<1>(t); z=std::get<2>(t); return *this;}

    inline vec3& operator+=(const vec3<R>& b) { x+=b.x; y+=b.y; z+=b.z; return (*this); }
    inline vec3& operator-=(const vec3<R>& b) { x-=b.x; y-=b.y; z-=b.z; return (*this); }
    inline vec3& operator*=(const vec3<R>& b) { x*=b.x; y*=b.y; z*=b.z; return (*this); }
    inline vec3& operator/=(const vec3<R>& b) { x/=b.x; y/=b.y; z/=b.z; return (*this); }
    inline vec3& operator*=(const R b) { x*=b;y*=b;z*=b; return (*this); }
    inline vec3& operator/=(const R b) { x/=b;y/=b;z/=b; return (*this); }

    inline R average() const { return (x + y + z) / 3; }
    inline R luminance() const { return (x * 0.299 + y * 0.587 + z * 0.114); }

    inline R operator[](int index) const { return ((R*)this)[index]; }
    inline R& operator[](int index) { return ((R*)this)[index]; }

    inline R lengthSqr() const { return dot(*this,*this); }
    inline R length() const { return sqrt(lengthSqr()); }
    inline uint8_t max_component_index() const { return (x>=y && x>=z) ? 0 : ((y>=x && y>=z) ? 1 : 2); }

    std::string print() const;

    inline bool iszero() const { return x == 0 && y == 0 && z == 0; }
    inline bool hasnan() const { return isnan(x) || isnan(y) || isnan(z); }
    inline bool hasinf() const { return isinf(x) || isinf(y) || isinf(z); }
    inline bool hasneg() const { return x < 0 || y < 0 || z < 0; }
    inline bool hasinvalid() const { return hasinf() || hasnan() || hasneg(); }

    inline std::tuple<R,R,R> unpack() const { return std::tuple<R,R,R>(x, y, z); }

    // Friend Functions (not member function)
    friend vec3 operator-(const vec3& a) { return vec3(-a.x,-a.y,-a.z); }
    friend vec3 operator+(const vec3& a, const vec3& b) { return vec3(a.x+b.x,a.y+b.y,a.z+b.z); }
    friend vec3 operator-(const vec3& a, const vec3& b) { return vec3(a.x-b.x,a.y-b.y,a.z-b.z); }
    friend vec3 operator*(const vec3& a, const vec3& b) { return vec3(a.x*b.x,a.y*b.y,a.z*b.z); }
    friend vec3 operator/(const vec3& a, const vec3& b) { return vec3(a.x/b.x,a.y/b.y,a.z/b.z); }
    friend vec3 operator*(const vec3& a, const R b) { return vec3(a.x*b,a.y*b,a.z*b); }
    friend vec3 operator/(const vec3& a, const R b) { return vec3(a.x/b,a.y/b,a.z/b); }
    friend vec3 operator*(const R a, const vec3& b) { return vec3<R>(a*b.x,a*b.y,a*b.z); }
    friend vec3 operator/(const R a, const vec3& b) { return vec3<R>(a/b.x,a/b.y,a/b.z); }
    friend bool operator==(const vec3& a, const vec3& b) { return a.x==b.x&&a.y==b.y&&a.z==b.z; }
    friend bool operator!=(const vec3& a, const vec3& b) { return !(a==b); }
    friend R dot(const vec3& a, const vec3& b) { return a.x*b.x+a.y*b.y+a.z*b.z; }
    friend vec3 abs_component(const vec3& a) { return vec3(abs(a.x), abs(a.y), abs(a.z)); }
    friend vec3 exp_component(const vec3& a) { return vec3(exp(a.x), exp(a.y), exp(a.z)); }
    friend vec3 pow_component(const vec3& a, R e) { return vec3(pow(a.x,e), pow(a.y,e), pow(a.z,e)); }
    friend vec3 sqrt_component(const vec3& a) { return vec3((R)sqrt(a.x), (R)sqrt(a.y), (R)sqrt(a.z)); }
    friend vec3 min_component(const vec3& a, const vec3& b) { return vec3(min(a.x,b.x),min(a.y,b.y),min(a.z,b.z)); }
    friend vec3 max_component(const vec3& a, const vec3& b) { return vec3(max(a.x,b.x),max(a.y,b.y),max(a.z,b.z)); }
    friend vec3 floor_component(const vec3& a) { return vec3(floor(a.x), floor(a.y), floor(a.z)); }
    friend vec3 ceil_component(const vec3& a) { return vec3(ceil(a.x), ceil(a.y), ceil(a.z)); }
    friend vec3 normalize(const vec3& a) { R l = sqrt(dot(a, a)); return (l == 0) ? a : a/l; }
};

template <typename R>
vec3<R> cross(const vec3<R>& a, const vec3<R>& b) {
    return vec3<R>(a.y*b.z-a.z*b.y,-a.x*b.z+a.z*b.x,a.x*b.y-a.y*b.x);
}

template <typename R>
inline std::string vec3<R>::print() const {
    char s[64]; sprintf(s,"[%f,%f,%f]",x,y,z); return std::string(s);
}
template <>
inline std::string vec3<int>::print() const {
    char s[64]; sprintf(s,"[%d,%d,%d]",x,y,z); return std::string(s);
}

template<typename R>
inline vec3<R> makevec3(R a) { return vec3<R>(a, a, a); }

template <typename R>
struct vec4 {
    R x, y, z, w;
    vec4() = default;
    vec4(R x, R y, R z, R w) : x(x), y(y), z(z), w(w) {}

    inline vec4& operator=(const std::tuple<R,R,R,R> &t) { x=std::get<0>(t); y=std::get<1>(t); z=std::get<2>(t); w=std::get<3>(t); return *this;}

    inline vec4& operator+=(const vec4<R>& b) { x+=b.x; y+=b.y; z+=b.z; w+=b.w; return (*this); }
    inline vec4& operator-=(const vec4<R>& b) { x-=b.x; y-=b.y; z-=b.z; w-=b.w; return (*this); }
    inline vec4& operator*=(const vec4<R>& b) { x*=b.x; y*=b.y; z*=b.z; w*=b.w; return (*this); }
    inline vec4& operator/=(const vec4<R>& b) { x/=b.x; y/=b.y; z/=b.z; w/=b.w; return (*this); }
    inline vec4& operator*=(const R b) { x*=b;y*=b;z*=b;w*=b; return (*this); }
    inline vec4& operator/=(const R b) { x/=b;y/=b;z/=b;w/=b; return (*this); }

    inline R operator[](int index) const { return ((R*)this)[index]; }
    inline R& operator[](int index) { return ((R*)this)[index]; }

    std::string print() const;

    inline bool iszero() const { return x == 0 && y == 0 && z == 0 && w == 0; }
    inline bool hasnan() const { return isnan(x) || isnan(y) || isnan(z) || isnan(w); }
    inline bool hasinf() const { return isinf(x) || isinf(y) || isinf(z) || isinf(w); }
    inline bool hasneg() const { return x < 0 || y < 0 || z < 0 || w < 0; }
    inline bool hasinvalid() const { return hasinf() || hasnan() || hasneg(); }

    inline std::tuple<R,R,R,R> unpack() const { return std::tuple<R,R,R,R>(x, y, z, w); }

    // Friend Functions (not member function)
    friend vec4 operator-(const vec4& a) { return vec4(-a.x,-a.y,-a.z,-a.w); }
    friend vec4 operator+(const vec4& a, const vec4& b) { return vec4(a.x+b.x,a.y+b.y,a.z+b.z,a.w+b.w); }
    friend vec4 operator-(const vec4& a, const vec4& b) { return vec4(a.x-b.x,a.y-b.y,a.z-b.z,a.w-b.w); }
    friend vec4 operator*(const vec4& a, const vec4& b) { return vec4(a.x*b.x,a.y*b.y,a.z*b.z,a.w*b.w); }
    friend vec4 operator/(const vec4& a, const vec4& b) { return vec4(a.x/b.x,a.y/b.y,a.z/b.z,a.w/b.w); }
    friend vec4 operator*(const vec4& a, const R b) { return vec4(a.x*b,a.y*b,a.z*b,a.w*b); }
    friend vec4 operator/(const vec4& a, const R b) { return vec4(a.x/b,a.y/b,a.z/b,a.w/b); }
    friend vec4 operator*(const R a, const vec4& b) { return vec4<R>(a*b.x,a*b.y,a*b.z,a*b.w); }
    friend vec4 operator/(const R a, const vec4& b) { return vec4<R>(a/b.x,a/b.y,a/b.z,a/b.w); }
    friend bool operator==(const vec4& a, const vec4& b) { return a.x==b.x&&a.y==b.y&&a.z==b.z&&a.w==b.w; }
    friend bool operator!=(const vec4& a, const vec4& b) { return !(a==b); }
    friend vec4 abs_component(const vec4& a) { return vec4(abs(a.x), abs(a.y), abs(a.z), abs(a.w)); }
    friend vec4 exp_component(const vec4& a) { return vec4(exp(a.x), exp(a.y), exp(a.z), exp(a.w)); }
    friend vec4 pow_component(const vec4& a, R e) { return vec4(pow(a.x,e), pow(a.y,e), pow(a.z,e), pow(a.w,e)); }
    friend vec4 sqrt_component(const vec4& a) { return vec4((R)sqrt(a.x), (R)sqrt(a.y), (R)sqrt(a.z), (R)sqrt(a.w)); }
    friend vec4 min_component(const vec4& a, const vec4& b) { return vec4(min(a.x,b.x),min(a.y,b.y),min(a.z,b.z),min(a.w,b.w)); }
    friend vec4 max_component(const vec4& a, const vec4& b) { return vec4(max(a.x,b.x),max(a.y,b.y),max(a.z,b.z),max(a.w,b.w)); }
    friend vec4 floor_component(const vec4& a) { return vec4(floor(a.x), floor(a.y), floor(a.z), floor(a.w)); }
    friend vec4 ceil_component(const vec4& a) { return vec4(ceil(a.x), ceil(a.y), ceil(a.z), ceil(a.w)); }
};

template <typename R>
inline std::string vec4<R>::print() const {
    char s[64]; sprintf(s,"[%f,%f,%f,%f]",x,y,z,w); return std::string(s);
}
template <>
inline std::string vec4<int>::print() const {
    char s[64]; sprintf(s,"[%d,%d,%d,%d]",x,y,z,w); return std::string(s);
}
template <>
inline std::string vec4<unsigned int>::print() const {
    char s[64]; sprintf(s,"[%u,%u,%u,%u]",x,y,z,w); return std::string(s);
}

template<typename R>
inline vec4<R> makevec4(R a) { return vec4<R>(a, a, a, a); }

typedef vec2<float>             vec2f;
typedef vec2<double>            vec2d;
typedef vec2<int>               vec2i;
typedef vec2<unsigned int>      vec2u;

typedef vec3<float>             vec3f;
typedef vec3<double>            vec3d;
typedef vec3<int>               vec3i;
typedef vec3<unsigned int>      vec3u;

typedef vec4<float>             vec4f;
typedef vec4<double>            vec4d;
typedef vec4<int>               vec4i;
typedef vec4<unsigned int>      vec4u;

// vec constants
const vec3f zero3f = vec3f(0.0f,0.0f,0.0f);
const vec3d zero3d = vec3d(0.0,0.0,0.0);
const vec3i zero3i = vec3i(0,0,0);
const vec3u zero3u = vec3u(0u,0u,0u);

const vec2f zero2f = vec2f(0.0f,0.0f);
const vec2d zero2d = vec2d(0.0,0.0);
const vec2i zero2i = vec2i(0,0);
const vec2u zero2u = vec2u(0u,0u);

const vec4f zero4f = vec4f(0.0f,0.0f,0.0f,0.0f);
const vec4d zero4d = vec4d(0.0,0.0,0.0,0.0);
const vec4i zero4i = vec4i(0,0,0,0);
const vec4u zero4u = vec4u(0u,0u,0u,0u);

const vec3f half3f = vec3f(0.5f,0.5f,0.5f);
const vec3d half3d = vec3d(0.5,0.5,0.5);

const vec2f half2f = vec2f(0.5f,0.5f);
const vec2d half2d = vec2d(0.5,0.5);

const vec4f half4f = vec4f(0.5f,0.5f,0.5f,0.5f);
const vec4d half4d = vec4d(0.5,0.5,0.5,0.5);

const vec3f one3f = vec3f(1.0f,1.0f,1.0f);
const vec3d one3d = vec3d(1.0,1.0,1.0);
const vec3i one3i = vec3i(1,1,1);
const vec3u one3u = vec3u(1u,1u,1u);

const vec2f one2f = vec2f(1.0f,1.0f);
const vec2d one2d = vec2d(1.0,1.0);
const vec2i one2i = vec2i(1,1);
const vec2u one2u = vec2u(1u,1u);

const vec4f one4f = vec4f(1.0f,1.0f,1.0f,1.0f);
const vec4d one4d = vec4d(1.0,1.0,1.0,1.0);
const vec4i one4i = vec4i(1,1,1,1);
const vec4u one4u = vec4u(1u,1u,1u,1u);

const vec3f x3f = vec3f(1,0,0);
const vec3f y3f = vec3f(0,1,0);
const vec3f z3f = vec3f(0,0,1);

const vec3d x3d = vec3d(1,0,0);
const vec3d y3d = vec3d(0,1,0);
const vec3d z3d = vec3d(0,0,1);

template<typename R>
inline R to1D(const vec2<R> &v, R width) {
    return v.x + v.y * width;
}

template<typename R>
inline vec2<R> to2D(R i, R width) {
    return { i % width, i / width };
}

#endif
