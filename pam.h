#ifndef PAM_H
#define PAM_H

#include "raw.h"

// see raw documentation
// only types supported are unsigned byte and float, for now
void loadPnm(const char *filename, rawtype& type,
             int& size1, int& size2, int& nc,
             rawbuffer& maxv, rawbuffer& buffer);
void savePnm(const char *filename, rawtype type,
             int width, int height, int nc,
             bool ascii, rawbuffer buffer);

Image<vec3f> loadPnm3f(const string& filename); // auto coverts to float
Image<float> loadPnmf(const string& filename); // auto coverts to float

void savePfm(const char* filename, const Image<vec3f>& image);
void savePfm(const string& filename, const Image<vec3f>& image);
void savePfm(const char* filename, const Image<float>& image);
void savePfm(const string& filename, const Image<float>& image);

template<typename R>
void savePfm(const char* filename, const Image<R>& image) {
    Image<float> fimage(image.width(), image.height());
    for (int i = 0; i < fimage.size(); i++) {
        fimage.at(i) = image.at(i);
    }
    savePfm(filename, fimage);
}

template<typename R>
void savePfm(const string& filename, const Image<R>& image) {
    Image<float> fimage(image.width(), image.height());
    for (int i = 0; i < fimage.size(); i++) {
        fimage.at(i) = image.at(i);
    }
    savePfm(filename, fimage);
}

template <int W, int H>
void savePfm(const char* filename, const SImage<vec3f, W, H>& image) {
    savePnm(filename, 'f', image.width(), image.height(), 3, false, (rawbuffer)&image.at(0));
}

template <int W, int H>
void savePfm(const string& filename, const SImage<vec3f, W, H>& image) {
    savePnm(filename.c_str(), 'f', image.width(), image.height(), 3, false, (rawbuffer)&image.at(0));
}

template <int W, int H>
void savePfm(const char* filename, const SImage<float, W, H>& image) {
    savePnm(filename, 'f', image.width(), image.height(), 1, false, (rawbuffer)&image.at(0));
}

template <int W, int H>
void savePfm(const string& filename, const SImage<float, W, H>& image) {
    savePnm(filename.c_str(), 'f', image.width(), image.height(), 1, false, (rawbuffer)&image.at(0));
}

#endif
