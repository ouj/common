#ifndef PROGRESS_H_
#define PROGRESS_H_

#include <cassert>
#include <stdio.h>
#include <cstdarg>
#include <string>
#include <tbb/tbb.h>
#include <stack>

struct DebugLevel {
    tbb::tick_count time_start;
    char *text_end = nullptr;
    bool newline = true;

    DebugLevel( bool newline = true ) : time_start(tbb::tick_count::now()), newline(newline) { }
};

class Progress {
public:
    void print( const char *fmt, ... ) {
        if( !levels.empty() && !levels.top()->newline ) {
            printf( "\n" );
            levels.top()->newline = true;
        }

        va_list args; va_start(args, fmt);
        _print( fmt, args );
        va_end(args);
    }

    void progress_start( const char *s_pre ) {
        if( !levels.empty() && !levels.top()->newline ) {
            printf( "\n" );
            levels.top()->newline = true;
        }

        progress_start_time = tbb::tick_count::now();

        if(false) {
            char s[256];
            _print_spaces(s); sprintf( &s[strlen(s)], "Progress: " );
            for( int i = strlen(s); i < sz_width; i++ ) sprintf( &s[i], "." );
            printf( "%s", s ); fflush(stdout);
        } else {
            _print_spaces();
        }

        if( s_pre ) printf( "%s", s_pre );
        _print_progress( 0.0f, false );
        fflush(stdin);
    }
    void progress_update( float p ) { _print_progress( p ); fflush(stdout); }
    void progress_end() {
        tbb::tick_count::interval_t time_delta = tbb::tick_count::now() - progress_start_time;
        _print_progress( 1.0 ); printf( "\b%0.2fs\n", time_delta.seconds() );
    }

    DebugLevel *start( const char *fmt, ... ) {
        if( !levels.empty() && !levels.top()->newline ) {
            printf( "\n" );
            levels.top()->newline = true;
        }

        va_list args; va_start(args, fmt);
        _print(fmt, args);
        va_end(args);

        DebugLevel *dlev = new DebugLevel();
        levels.push( dlev );
        depth++;
        return dlev;
    }

    DebugLevel *start_single( const char *fmt, ... ) {
        if( !levels.empty() && !levels.top()->newline ) {
            printf( "\n" );
            levels.top()->newline = true;
        }

        char s[256];
        va_list args; va_start(args, fmt);
        _print_spaces(s);
        vsprintf( &s[strlen(s)], fmt, args );
        va_end(args);

        for( int i = strlen(s); i < sz_width; i++ ) sprintf( &s[i], "." );
        printf( "%s", s ); fflush(stdout);

        DebugLevel *dlev = new DebugLevel(false);
        levels.push( dlev );
        depth++;
        return dlev;
    }

    template<typename T>
    void fn( const char *fmt, T fn) {
        start(fmt); fn(); end();
    }

    template<typename T>
    void fn_quiet( const char *fmt, T fn ) {
        start(fmt); fn(); end_quiet();
    }

    template<typename T>
    void fn_single( const char *fmt, T fn ) {
        start_single(fmt); fn(); end();
    }

    void end() {
        assert( !levels.empty() );
        DebugLevel *dlevel = levels.top();
        tbb::tick_count::interval_t time_delta = tbb::tick_count::now() - dlevel->time_start;

        if( dlevel->newline ) {
            if( dlevel->text_end ) print( "%s %0.2fs", dlevel->text_end, time_delta );
            else print( "%0.2fs", time_delta.seconds() );
        } else {
            printf( "%0.2fs\n", time_delta.seconds());
        }

        levels.pop(); depth--;
        delete dlevel;
    }

    void end_quiet() {
        assert( !levels.empty() );
        DebugLevel *dlevel = levels.top();
        levels.pop(); depth--;
        if( !dlevel->newline ) printf("\n");
        delete dlevel;
    }
private:
    const char *s_spinner = "|/-\\";
    const char *s_level = "  ";
    const int n_s_level = strlen(s_level);

    int i_spinner = 0;
    int depth = 0;
    std::stack<DebugLevel*> levels;

    const int sz_width = 60;
    const int sz_progress = 50;
    tbb::tick_count progress_start_time;

    void _print_spaces() { for(int i = 0; i < depth; i++ ) printf( "%s", s_level ); }
    void _print_spaces( char *s ) {
        s[0] = 0; for(int i = 0; i < depth; i++ ) sprintf( &s[i*n_s_level], "%s", s_level );
    }
    void _print( const char *fmt, va_list args ) {
        _print_spaces();
        vprintf( fmt, args );
        printf("\n");
    }
    void _print_progress( float p, bool clear=true ) {
        p = max(0.0f,min(1.0f,p));
        int n_s = (int)((float)sz_progress*p);
        int i_p = (int)( 100.0f * p );

        if( clear ) {
            printf( "\b\b\b\b\b\b\b\b" );
            for( int i = 0; i < sz_progress; i++ ) printf( "\b" );
            printf( "\b" );
        }

        printf( "[" );
        for( int i = 0; i < n_s; i++ ) printf("*");
        for( int i = n_s; i < sz_progress; i++ ) printf(" ");
        printf( "] %03d%% %c", i_p, s_spinner[i_spinner] );
        i_spinner = (i_spinner+1)%4;
    }
};

#endif

