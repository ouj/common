#ifndef _QUATERNION_H_
#define _QUATERNION_H_

#include "stdmath.h"
#include "vec.h"

template<typename R>
struct quaternion {
    R x, y, z, w;
    quaternion() {}
    quaternion(R x, R y, R z, R w) : x(x), y(y), z(z), w(w) {}
    quaternion(const vec3<R> &axis, R angle);
    frame3<R> toframe() const;
};

template <typename R>
quaternion<R>::quaternion(const vec3<R> & axis, R angle) {
    R s = sin(angle * 0.5f);
    w = cos(angle * 0.5f);
    x = axis.x * s;
    y = axis.y * s;
    z = axis.z * s;
}

template <typename R>
frame3<R> quaternion<R>::toframe() const {
    float xx = x * x, yy = y * y, zz = z * z;
    float xy = x * y, xz = x * z, yz = y * z;
    float wx = x * w, wy = y * w, wz = z * w;
    frame3<R> f;
    f.o = vec3<R>(0, 0, 0);
    f.x = vec3<R>(1.0 - 2.0 * (yy + zz), 2.0 * (xy + wz),       2.0 * (xz - wy));
    f.y = vec3<R>(2.0 * (xy - wz),       1.0 - 2.0 * (xx + zz), 2.0 * (yz + wx));
    f.z = vec3<R>(2.0 * (xz + wy),       2.0 * (yz - wx),       1.0 - 2.0 * (xx + yy));
    return f;
};

template <typename R>
R dot(const quaternion<R> &a, const quaternion<R> &b) {
    return a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w;
}

template <typename R>
quaternion<R> operator/(const quaternion<R> &qq, R l) {
    quaternion<R> q;
    q.x = qq.x / l; q.y = qq.y / l; q.z = qq.z / l; q.w = qq.w / l;
    return q;
}

template <typename R>
quaternion<R> operator*(const quaternion<R> &qq, float f) {
    quaternion<R> q;
    q.x = qq.x * f; q.y = qq.y * f; q.z = qq.z * f; q.w = qq.w * f;
    return q;
}

template <typename R>
quaternion<R> operator+(const quaternion<R> &q1, const quaternion<R> &q2) {
    quaternion<R> q;
    q.x = q1.x + q2.x; q.y = q1.y + q2.y; q.z = q1.z + q2.z; q.w = q1.w + q2.w;
    return q;
}

template <typename R>
quaternion<R> operator-(const quaternion<R> &q1, const quaternion<R> &q2) {
    quaternion<R> q;
    q.x = q1.x - q2.x; q.y = q1.y - q2.y; q.z = q1.z - q2.z; q.w = q1.w - q2.w;
    return q;
}

template <typename R>
quaternion<R> normalize(const quaternion<R> &qq) {
    R l = sqrt(dot(qq, qq));
    if(l != 0) return qq / l;
    else return qq;
}

template <typename R>
quaternion<R> slerp(R t, const quaternion<R> &q1, const quaternion<R> &q2) {
    float cost = dot(q1, q2);
    if (cost > .9995f) return normalize(q1 * (1.0 - t) + q2 * t);
    else {
        float theta = acos(clamp(cost, (R)-1, (R)1));
        float thetap = theta * t;
        quaternion<R> qperp = normalize(q2 - q1 * cost);
        return q1 * cos(thetap) + qperp * sin(thetap);
    }
}


typedef quaternion<float> quaternionf;
typedef quaternion<double> quaterniond;

#endif // _QUATERNION_H_
