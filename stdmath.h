#ifndef STDMATH_H_
#define STDMATH_H_

#include <limits>
#include <algorithm>
#include <cmath>
#include <stdint.h>

using std::min;
using std::max;

using std::pow;
using std::cos;
using std::sin;
using std::abs;
using std::exp;
using std::log;
using std::modf;

using std::numeric_limits;

// check for nans and inf
#ifdef WIN32
#include <float.h>
#ifndef isnan
#define isnan(a) _isnan(a)
#endif
#ifndef isinf
#define isinf(a) (!_finite(a))
#endif
#elif __APPLE__
using std::isnan;
using std::isinf;
#endif

template<typename R>
struct consts {
    static const R halfpi;
    static const R pi;
    static const R twopi;
    static const R fourpi;
    static const R inv_pi;
    static const R inv_twopi;
    static const R inv_fourpi;

    static const R sqrttwo;

    static const R epsilon;
    static const R max;
    static const R min;
};

// memory unit conversion
template<typename R> inline R byteToKB(R s) { return s / (R)1024; }
template<typename R> inline R byteToMB(R s) { return s / ((R)1024 * (R)1024); };
template<typename R> inline R byteToGB(R s) { return s / ((R)1024 * (R)1024 * (R)1024); };

template<typename R> inline R gbToByte(R s) { return s * (R)1024; }
template<typename R> inline R mbToByte(R s) { return s * ((R)1024 * (R)1024); };
template<typename R> inline R kbToByte(R s) { return s * ((R)1024 * (R)1024 * (R)1024); };

// time unit conversion
template<typename R> inline R milliToSecond(R s) { return s / (R)1000; }
template<typename R> inline R microToSecond(R s) { return s / ((R)1000 * (R)1000); };
template<typename R> inline R nanoToSecond(R s) { return s / ((R)1000 * (R)1000 * (R)1000); };

template<typename R> inline R secondToMilli(R s) { return s * (R)1000; }
template<typename R> inline R secondToMicro(R s) { return s * ((R)1000 * (R)1000); };
template<typename R> inline R secondToNano(R s) { return s * ((R)1000 * (R)1000 * (R)1000); };

template<typename R> inline R nanoToMicro(R s) { return s / (R)1000; };
template<typename R> inline R microToMilli(R s) { return s / (R)1000; };
template<typename R> inline R nanoToMilli(R s) { return s / ((R)1000 * (R)1000); };

template<typename R> inline R milliToMicro(R s) { return s * (R)1000; };
template<typename R> inline R microToNano(R s) { return s * (R)1000; };
template<typename R> inline R milliToNano(R s) { return s * ((R)1000 * (R)1000); };

#endif
