#ifndef MONTECARLO_H
#define MONTECARLO_H

#include "stdmath.h"
#include "vec.h"
#include "func.h"
#include "array.h"

template<typename R>
inline R powerHeuristic(R fPdf, R gPdf) {
    R nf = 1; R ng = 1;
    R f = nf * fPdf; R g = ng * gPdf;
    return (f*f) / (f*f + g*g);
}

template<typename R>
inline int sampleUniform(R s, int size) {
    int l = clamp((int)(s * size), 0, size-1);
    return l;
}

template<typename R>
inline R sampleUniformPdf(int size) {
    return R(1) / size;
}

template<typename R>
inline R sampleHemisphericalCosPdf(const vec3<R>& w) {
    if(w.z <= 0) return 0;
    else return w.z/consts<R>::pi;
}

template<typename R>
inline vec3<R> sampleHemisphericalCos(const vec2<R>& ruv) {
    R z = sqrt(ruv.y);
    R r = sqrt(1-z*z);
    R phi = 2 * consts<R>::pi * ruv.x;
    return vec3<R>(r*cos(phi), r*sin(phi), z);
}

template<typename R>
inline R sampleHemisphericalCosPowerPdf(const vec3<R>& w, R n) {
    if(w.z <= 0)return 0;
    else return pow(w.z,n) * (n+1) / (2*consts<R>::pi);
}

template<typename R>
inline vec3<R> sampleHemisphericalCosPower(const vec2<R>& ruv, R n) {
    R z = pow(ruv.y,1/(n+1));
    R r = sqrt(1-z*z);
    R phi = 2 * consts<R>::pi * ruv.x;
    return vec3<R>(r*cos(phi), r*sin(phi), z);
}

template<typename R>
inline R sampleSphericalPdf(const vec3<R>& w) {
    return consts<R>::inv_fourpi;
}

template<typename R>
inline vec3<R> sampleSpherical(const vec2<R>& ruv) {
    R z = 2*ruv.y-1;
    R r = sqrt(1-z*z);
    R phi = 2 * consts<R>::pi * ruv.x;
    return vec3<R>(r*cos(phi), r*sin(phi), z);
}

template<typename R>
inline R sampleHemisphericalPdf(const vec3<R>& w) {
    if(w.z <= 0 ) return 0;
    else return consts<R>::inv_twopi;
}

template<typename R>
inline vec3<R> sampleHemispherical(const vec2<R>& ruv) {
    R z = ruv.y;
    R r = sqrt(1-z*z);
    R phi = 2 * consts<R>::pi * ruv.x;
    return vec3<R>(r*cos(phi), r*sin(phi), z);
}

// from pbrt
template<typename R>
inline vec2<R> sampleConcentricDisk(const vec2<R> &ruv) {
    R r, theta;
    // Map uniform random numbers to $[-1,1]^2$
    R sx = 2 * ruv.x - 1;
    R sy = 2 * ruv.y - 1;

    // Map square to $(r,\theta)$
    // Handle degeneracy at the origin
    if (sx == 0 && sy == 0)
        return zero2f;
    if (sx >= -sy) {
        if (sx > sy) {
            // Handle first region of disk
            r = sx;
            if (sy > 0) theta = sy/r;
            else        theta = 8 + sy/r;
        }
        else {
            // Handle second region of disk
            r = sy;
            theta = 2 - sx/r;
        }
    }
    else {
        if (sx <= sy) {
            // Handle third region of disk
            r = -sx;
            theta = 4 - sy/r;
        }
        else {
            // Handle fourth region of disk
            r = -sy;
            theta = 6 + sx/r;
        }
    }
    theta *= consts<R>::pi / 4;
    return vec2<R>(r * cos(theta), r * sin(theta));
}

#endif
