#ifndef FRAME_H
#define FRAME_H

#include "stdmath.h"
#include "vec.h"
#include "ray.h"
#include "range.h"

template <typename R>
struct frame3 {
    vec3<R> o, x, y, z;
    frame3() {}
    frame3(const vec3<R>& o, const vec3<R>& x,
           const vec3<R>& y, const vec3<R>& z) : o(o), x(x), y(y), z(z){}

    vec3<R> transformVector(const vec3<R>& v) const { return x*v.x + y*v.y + z*v.z; }
    vec3<R> transformNormal(const vec3<R>& v) const { return x*v.x + y*v.y + z*v.z; }
    vec3<R> transformPoint(const vec3<R>& v) const { return o + x*v.x + y*v.y + z*v.z; }
    ray3<R> transformRay(const ray3<R>& r) const;
    range3<R> transformRange(const range3<R>& b) const;

    vec3<R> transformVectorInverse(const vec3<R>& v) const { return vec3<R>(dot(x,v), dot(y,v), dot(z,v)); }
    vec3<R> transformNormalInverse(const vec3<R>& v) const { return vec3<R>(dot(x,v), dot(y,v), dot(z,v)); }
    vec3<R> transformPointInverse(const vec3<R>& v) const { vec3<R> vv = v-o; return vec3<R>(dot(x,vv), dot(y,vv), dot(z,vv)); }
    ray3<R> transformRayInverse(const ray3<R>& r) const;
    frame3<R> transformFrame(const frame3<R>& v) const;
    range3<R> transformRangeInverse(const range3<R>& b) const;

    std::string print() const;
};

template <typename R>
std::string frame3<R>::print() const {
    char s[128];
    sprintf(s, "{o:%s,x:%s,y:%s,z:%s}",
            o.print().c_str(), x.print().c_str(),
            y.print().c_str(), z.print().c_str());
    return std::string(s);
}

template <typename R>
inline ray3<R> frame3<R>::transformRay(const ray3<R>& r) const {
    return ray3<R>(transformPoint(r.o),transformVector(r.d),r.mint,r.maxt);
}

template <typename R>
inline ray3<R> frame3<R>::transformRayInverse(const ray3<R>& r) const {
    return ray3<R>(transformPointInverse(r.o),transformVectorInverse(r.d),r.mint,r.maxt);
}

template <typename R>
inline frame3<R> frame3<R>::transformFrame(const frame3<R>& v) const {
    return frame3<R>(transformPoint(v.o),transformVector(v.x),
                     transformVector(v.y),transformVector(v.z));
}

template<typename R>
inline range3<R> frame3<R>::transformRange(const range3<R>& b) const {
    range3<R> ret = invalidrange3<R>();
    ret.grow(transformPoint(vec3f(b.min.x, b.min.y, b.min.z)));
    ret.grow(transformPoint(vec3f(b.min.x, b.min.y, b.max.z)));
    ret.grow(transformPoint(vec3f(b.min.x, b.max.y, b.min.z)));
    ret.grow(transformPoint(vec3f(b.min.x, b.max.y, b.max.z)));
    ret.grow(transformPoint(vec3f(b.max.x, b.min.y, b.min.z)));
    ret.grow(transformPoint(vec3f(b.max.x, b.min.y, b.max.z)));
    ret.grow(transformPoint(vec3f(b.max.x, b.max.y, b.min.z)));
    ret.grow(transformPoint(vec3f(b.max.x, b.max.y, b.max.z)));
    return ret;
}

// this is not a bijection, but conservative
template<typename R>
inline range3<R> frame3<R>::transformRangeInverse(const range3<R>& b) const {
    range3<R> ret = invalidrange3<R>();
    ret.grow(transformPointInverse(vec3f(b.min.x, b.min.y, b.min.z)));
    ret.grow(transformPointInverse(vec3f(b.min.x, b.min.y, b.max.z)));
    ret.grow(transformPointInverse(vec3f(b.min.x, b.max.y, b.min.z)));
    ret.grow(transformPointInverse(vec3f(b.min.x, b.max.y, b.max.z)));
    ret.grow(transformPointInverse(vec3f(b.max.x, b.min.y, b.min.z)));
    ret.grow(transformPointInverse(vec3f(b.max.x, b.min.y, b.max.z)));
    ret.grow(transformPointInverse(vec3f(b.max.x, b.max.y, b.min.z)));
    ret.grow(transformPointInverse(vec3f(b.max.x, b.max.y, b.max.z)));
    return ret;
}


template<typename R>
inline frame3<R> translationframe3(const vec3<R>& t) {
    return frame3<R>(t, vec3<R>(1,0,0), vec3<R>(0,1,0), vec3<R>(0,0,1));
}

template<typename R>
inline frame3<R> rotationframe3(const vec3<R>& axis, R angle) {
    vec3<R> v = normalize(axis);
    R c = cos(angle); R s = sin(angle);
    return frame3<R>(vec3<R>(0,0,0),
                         vec3<R>(c+(1-c)*v.x*v.x,(1-c)*v.x*v.y+v.z*s,(1-c)*v.x*v.z-v.y*s),
                         vec3<R>((1-c)*v.x*v.y-v.z*s,c+(1-c)*v.y*v.y,(1-c)*v.y*v.z+v.x*s),
                         vec3<R>((1-c)*v.x*v.z+v.y*s,(1-c)*v.y*v.z-v.x*s,c+(1-c)*v.z*v.z));
}

template<typename R>
inline frame3<R> invert(const frame3<R>& f) {
    frame3<R> i = frame3<R>(zero3f,
                            vec3<R>(f.x.x,f.y.x,f.z.x),
                            vec3<R>(f.x.y,f.y.y,f.z.y),
                            vec3<R>(f.x.z,f.y.z,f.z.z));
    i.o = -i.transformPoint(f.o);
    return i;
}

template <typename R>
inline frame3<R> flip(const frame3<R>& f) {
    frame3<R> ff;
    ff.o = f.o;
    ff.x = -f.y;
    ff.y = -f.x;
    ff.z = -f.z;
    return ff;
}

typedef frame3<float> frame3f;
typedef frame3<double> frame3d;

template<typename R>
inline frame3<R> makeframe3(const vec3<R> &pos, const vec3<R> &norm) {
    frame3<R> f;
    f.o = pos;
    f.z = norm;
    xyFromZ(f.z, f.x, f.y);
    return f;
};

inline frame3f translationframe3f(const vec3f& t) {
    return translationframe3(t);
}

inline frame3f rotationframe3f(const vec3f& axis, float angle) {
    return rotationframe3(axis,angle);
}

const frame3f defaultframe3f = frame3f { zero3f, x3f, y3f, z3f };

#endif
