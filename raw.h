#ifndef RAW_H
#define RAW_H

#include "array.h"
#include "vec.h"
#include <string>
#include <vector>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
using std::string;

// modeled loosely on the python array module
// uses simple chars to indicate type information
// accepted types
// b/B -> signed/unsigned byte
// i/I -> signed/unsigned int
// f -> float
// add more later

typedef char rawtype;
typedef unsigned char* rawbuffer;

// low level file handling
void loadRaw(const string& filename, rawtype& type,
             int& size1, int& size2, int& nc,
             rawbuffer& buffer);
void saveRaw(const string& filename, rawtype type,
             int size1, int size2, int nc,
             rawbuffer buffer);
void loadDim(const string& filename, rawtype& type,
             int& size1, int& size2, int& nc);

// high level file handling -- add more if needed
void loadRaw(const string& filename, std::vector<float>& array);
void loadRaw(const string& filename, std::vector<vec2f>& array);
void loadRaw(const string& filename, std::vector<vec3f>& array);
void loadRaw(const string& filename, std::vector<int>& array);
void loadRaw(const string& filename, std::vector<vec3i>& array);
void loadRaw(const string& filename, std::vector<vec2i>& array);
void loadRaw(const string& filename, Image<float>& array);
void loadRaw(const string& filename, Image<vec3f>& array);

void saveRaw(const string& filename, const std::vector<vec3f>& array);
void saveRaw(const string& filename, const std::vector<vec3i>& array);
void saveRaw(const string& filename, const std::vector<vec2f>& array);

void saveRaw(const string& filename, const std::vector<vec3f>& array);
void saveRaw(const string& filename, const std::vector<vec3i>& array);
void saveRaw(const string& filename, const std::vector<vec2f>& array);


template<typename T>
void writeRaw(FILE *f, const std::vector<T> &data) {
    size_t size = data.size();
    error_if_not(fwrite(&size, sizeof(size_t), 1, f) == sizeof(size_t), "invalid write");
    error_if_not(fwrite(data.data(), sizeof(T), size, f) == sizeof(T) * size, "invalid write");
}

template<typename T>
void readRaw(FILE *f, std::vector<T> &data) {
    size_t size = 0;
    error_if_not(fread(&size, sizeof(size_t), 1, f) == sizeof(size_t), "invalid read");
    data.resize(size);
    error_if_not(fread(data.data(), sizeof(T), size, f) == sizeof(T) * size, "invalid write");
}

template<typename T>
void saveArray(const char* filename, const std::vector<T>& array) {
    const size_t batchsize = gbToByte(1); // 1 giga bytes.
    int fd = open(filename, O_WRONLY|O_CREAT, 0666);
    error_if_va(fd == -1, "cannot create file %s", filename);
    size_t s = array.size();
    ssize_t ret = write(fd, &s, sizeof(size_t));
    error_if_va(ret != sizeof(size_t), "cannot write size %s", filename);
    size_t bytes = s * sizeof(T);
    char* ptr = (char*)&array[0];
    while(bytes > 0) {
        size_t towrite = min<size_t>(bytes, batchsize);
        int ret = write(fd, ptr, towrite);
        error_if_va(ret != towrite, "cannot write %s", filename);
        bytes -= towrite;
    }
    close(fd);
}

template<typename T>
std::vector<T> loadArray(const char* filename) {
    int fd = open(filename, O_RDONLY);
    error_if_va(fd == -1, "cannot open file %s", filename);
    size_t s = 0;
    ssize_t ret = read(fd, &s, sizeof(size_t));
    error_if_va(ret != sizeof(size_t), "failed reading size %s", filename);
    std::vector<T> array(s);
    ret = read(fd, &array[0], s * sizeof(T));
    error_if_va(ret != s * sizeof(T), "failed reading %s", filename);
    close(fd);
    return array;
}

template<typename T>
void saveArray(const std::string &filename, const std::vector<T> &array) { saveArray<T>(filename.c_str(), array); }
template<typename T>
std::vector<T> loadArray(const std::string &filename) { return loadArray<T>(filename.c_str()); }

#endif
