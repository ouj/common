#ifndef RAY_H
#define RAY_H

#include "vec.h"

template <typename R>
struct ray3 {
    static R epsilon;
    static R infinity;

    vec3<R> o; // origin
    vec3<R> d; // direction
    R mint, maxt; // bounds
    ray3() {}
    ray3(const vec3<R>& o, const vec3<R>& d,
         R mint = ray3<R>::epsilon, R maxt = ray3<R>::infinity)
         : o(o), d(d), mint(mint), maxt(maxt) {}
    inline vec3<R> eval(R t) const { return o + d * t; }
};

template<typename R> R ray3<R>::epsilon = R(1e-5);
template<typename R> R ray3<R>::infinity = numeric_limits<R>::infinity();

typedef ray3<float> ray3f;
typedef ray3<double> ray3d;

template<typename R>
inline ray3<R> makeraysegment3(const vec3<R>& p1, const vec3<R>& p2,
                        R mint = ray3<R>::epsilon) {
    return ray3<R>(p1, normalize(p2-p1), mint, (p2-p1).length()-mint);
}

inline ray3f makeraysegment3f(const vec3f& p1, const vec3f& p2, float mint = ray3f::epsilon) {
    return makeraysegment3<float>(p1, p2, mint);
}

const ray3f defaultray3f = ray3f { zero3f, z3f, 0.0f, 0.0f };

#endif
