#ifndef BSONIO_H
#define BSONIO_H

#include "json.h"

jsonValue loadBin(const std::string& filename);
void saveBin(const std::string& filename, const jsonValue& json);

#endif
