#ifndef MORTON_H
#define MORTON_H
#include <tuple>
#include <stdint.h>
#include "vec.h"

namespace morton2d {
    inline int compactbit(uint x, int offset) {
        static const uint B[] = { 0x0000ffffu, 0x00ff00ffu, 0x0f0f0f0fu, 0x33333333u, 0x55555555u };
        x = ( x >> offset ) & B[4];
        x = (x | (x >> 1)) & B[3];
        x = (x | (x >> 2)) & B[2];
        x = (x | (x >> 4)) & B[1];
        x = (x | (x >> 8)) & B[0];
        return x;
    }
    inline unsigned int spreadbit(uint x, short offset) {
        static const uint B[] = { 0x0000ffffu, 0x00ff00ffu, 0x0f0f0f0fu, 0x33333333u, 0x55555555u };
        x &= B[0];
        x = (x | (x << 8)) & B[1];
        x = (x | (x << 4)) & B[2];
        x = (x | (x << 2)) & B[3];
        x = (x | (x << 1)) & B[4];
        return x << offset;
    }
    inline uint encode(uint i, uint j) {
        return spreadbit(i, 0) | spreadbit(j, 1);
    }
    inline std::tuple<uint, uint> decode(uint code) {
        int i, j;
        i = compactbit(code, 0);
        j = compactbit(code, 1);
        return std::tuple<int, int>(i, j);
    }
}


namespace morton3d {
    inline int compactbit(uint x, int offset) {
        static const uint B[] = { 0x000003ffu, 0x030000ffu, 0x0300f00fu, 0x030c30c3u, 0x09249249u };
        x = ( x >> offset ) & B[4];
        x = (x | (x >>  2)) & B[3];
        x = (x | (x >>  4)) & B[2];
        x = (x | (x >>  8)) & B[1];
        x = (x | (x >> 16)) & B[0];
        return x;
    }
    inline unsigned int spreadbit(uint x, short offset) {
        static const uint B[] = { 0x000003ffu, 0x030000ffu, 0x0300f00fu, 0x030c30c3u, 0x09249249u };
        x &= B[0];
        x = (x | (x << 16)) & B[1];
        x = (x | (x <<  8)) & B[2];
        x = (x | (x <<  4)) & B[3];
        x = (x | (x <<  2)) & B[4];
        return x << offset;
    }
    inline uint encode(uint i, uint j, uint k) {
        return spreadbit(i, 0) | spreadbit(j, 1) | spreadbit(k, 2);
    }
    inline uint encode(vec3i &v) { return encode(v.x, v.y, v.z); }
    inline std::tuple<uint, uint, uint> decode(uint code) {
        int i, j, k;
        i = compactbit(code, 0);
        j = compactbit(code, 1);
        k = compactbit(code, 2);
        return std::tuple<int, int, int>(i, j, k);
    }
}

#endif //MORTON_H