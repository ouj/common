#ifndef ARRAY_H
#define ARRAY_H

#include "debug.h"
#include <algorithm>
#include "vec.h"
#include <vector>
#include <array>

//#define ARRAY_CHECK
#ifdef ARRAY_CHECK
template <typename T> inline void _check_array_access(const T& a, int i) { error_if_not_va(i >= 0 && i < a.size(), "index out of size bounds(%d/%d(", i, a.size()); }
template <typename T> inline void _check_array_capacity(const T& a, int i) { error_if_not_va(i >= 0 && i < a.capacity(), "index out of capacity bounds(%d/%d)", i, a.capacity()); }
template <typename T> inline void _check_array_newcapacity(const T& a, int c) { error_if_not_va(c >= 0 && c >= a.capacity(), "new capacity out of capacity bounds(%d/%d", c, a.capacity()); }
template <typename T> inline void _check_array2_access(const T& a, int i, int j) { error_if_not_va(i >= 0 && i < a.width() && j >= 0 && j < a.height(), "2d index out of size bounds(%d/%d,%d/%d)",i,a.width(),j,a.height()); }
template <typename T> inline void _check_array3_access(const T& a, int i, int j, int k) { error_if_not_va(i >= 0 && i < a.size1() && j >= 0 && j < a.size2() && k >= 0 && k < a.size2(), "2d index out of size bounds(%d/%d,%d/%d,%d/%d)",i,a.width(),j,a.height(),k,a.depth()); }
#else
// disable checks
#define _check_array_access(a,i)
#define _check_array_capacity(a,i)
#define _check_array_newcapacity(a,c)
#define _check_array2_access(a,i,j)
#define _check_array3_access(a,i,j,k)
#endif

template <typename T>
inline size_t size_in_bytes(const std::vector<T> vec) {
    return vec.size() * sizeof(T);
}

template <typename T>
struct Image {
    typedef T value_type;

    Image() = default;
    explicit Image(const vec2i dim) : Image(dim.x, dim.y) {}
    explicit Image(int l1, int l2) { _l1 = 0; _l2 = 0; _d = 0; realloc(l1, l2); }
    explicit Image(int l1, int l2, const T& v) { _l1 = 0; _l2 = 0; _d = 0; realloc(l1, l2); assign(v); }
    Image(const Image<T>& v) { _l1 = 0; _l2 = 0; _d = 0; assign(v); }
    Image(Image<T>&& v) noexcept { _l1 = v._l1; _l2 = v._l2; _d = v._d; v._l1 = 0; v._l2 = 0; v._d = 0; }

    ~Image() { clear(); }

    Image<T>& operator= (const Image<T>& v) { assign(v); return *this; }
    Image<T>& operator= (Image<T>&& v) noexcept { std::swap(_l1, v._l1); std::swap(_l2, v._l2); std::swap(_d, v._d); return *this; }

    int size() const { return _l1 * _l2; }
    bool empty() const { return _l1 == 0 || _l2 == 0; }

    int width() const { return _l1; }
    int height() const { return _l2; }

    void clear() { if(_d) delete [] _d; _l1 = 0; _l2 = 0; _d = 0; }
    void realloc(int l1, int l2) {
        if(l1 == _l1 && l2 == _l2) return;
        if(_d) delete [] _d;
        _l1 = l1; _l2 = l2; _d = new T[_l1 * _l2];
    }
    void resize(int l1, int l2) {
        if(l1 == _l1 && l2 == _l2) return;
        if(_l1 == 0 && _l2 == 0) return realloc(l1, l2);
        Image<T> newarray(l1, l2);
        for(int j = 0; j < std::min(_l2,l2); j ++) {
            for(int i = 0; i < std::min(_l1,l1); i ++) {
                newarray.at(i,j) = at(i,j);
            }
        }
        std::swap(newarray._d,_d);
        std::swap(newarray._l1,_l1);
        std::swap(newarray._l2,_l2);
    }

    T& at(int i) { return _d[i]; }
    const T& at(int i) const { return _d[i]; }
    T& at(int i, int j) { return _d[j*_l1+i]; }
    const T& at(int i, int j) const { _check_array2_access(*this, i, j); return _d[j*_l1+i]; }
    T& operator[](int i) { _check_array_access(*this, i); return _d[i]; }
    const T& operator[](int i) const { _check_array_access(*this, i); return _d[i]; }

    void assign(const T& v) { for(int i = 0; i < size(); i ++) _d[i] = v; }
    void assign(int l1, int l2, const T& v) { realloc(l1, l2); assign(v); }
    void assign(const Image<T>& v) { realloc(v.width(),v.height()); for(int i = 0; i < size(); i ++) _d[i] = v._d[i]; }

    int _l1 = 0;
    int _l2 = 0;
    T* _d = 0;
};

template <typename T, int W, int H>
struct SImage {
    typedef T value_type;

    SImage() = default;
    explicit SImage(const T& v) { assign(v); }
    SImage(const SImage& v) : _d(v._d) {}

    ~SImage() = default;
    SImage& operator= (const SImage& v) { _d = v._d; return *this; }

    constexpr int size() const { return W * H; }
    constexpr bool empty() const { return W == 0 || H == 0; }

    constexpr int width() const { return W; }
    constexpr int height() const { return H; }

    T& at(int i) { return _d.at(i); }
    const T& at(int i) const { return _d.at(i); }
    T& at(int i, int j) { return _d.at(j*W+i); }
    const T& at(int i, int j) const { return _d[j*W+i]; }
    T& operator[](int i) { return _d[i]; }
    const T& operator[](int i) const { return _d[i]; }

    void assign(const T& v) { _d.fill(v); }
    void assign(const SImage& v) { _d = v._d; }

    std::array<T, W*H> _d;
};

template<typename T> using sarray2 = Image<T>;

template <typename T>
struct Cube {
    typedef T value_type;

    Cube() { _l1 = 0; _l2 = 0; _l3 = 0; _d = 0; }
    explicit Cube(int l1, int l2, int l3) { _l1 = 0; _l2 = 0; _l3 = l3; _d = 0; realloc(l1, l2, l3); }
    explicit Cube(int l1, int l2, int l3, const T& v) { _l1 = 0; _l2 = 0; _l3 = l3; _d = 0; realloc(l1, l2, l3); assign(v); }
    Cube(const Cube<T>& v) { _l1 = 0; _l2 = 0; _l3 = 0; _d = 0; assign(v); }
    Cube(Cube<T>&& v) noexcept { _l1 = v._l1; _l2 = v._l2; _l3 = v._l3; _d = v._d; v._l1 = 0; v._l2 = 0; v._l3 = 0; v._d = 0; }

    ~Cube() { clear(); }

    Cube<T>& operator= (const Cube<T>& v) { assign(v); return *this; }
    Cube<T>& operator= (Cube<T>&& v) noexcept { std::swap(_l1, v._l1); std::swap(_l2, v._l2); std::swap(_l3, v._l3); std::swap(_d, v._d); return *this; }

    int size() const { return _l1 * _l2 * _l3; }
    bool empty() const { return _l1 == 0 || _l2 == 0 || _l3 == 0; }

    int width() const { return _l1; }
    int height() const { return _l2; }
    int depth() const { return _l3; }

    void clear() { if(_d) delete [] _d; _l1 = 0; _l2 = 0; _d = 0; }
    void realloc(int l1, int l2, int l3) {
        if(l1 * l2 * l3 == _l1 * _l2 * _l3) {
            _l1 = l1; _l2 = l2; _l3 = l3;
            return;
        }
        if(_d) delete [] _d;
        _l1 = l1; _l2 = l2; _l3 = l3; _d = new T[_l1 * _l2 * _l3];
    }
    void resize(int l1, int l2, int l3) {
        if(l1 * l2 * l3 == _l1 * _l2 * _l3) {
            _l1 = l1; _l2 = l2; _l3 = l3;
            return;
        }
        if(_l1 == 0 || _l2 == 0 || _l3 == 0) clear();
        Cube<T> newarray(l1, l2, l3);
        for(int k = 0; k < std::min(_l3,l3); k ++) {
            for(int j = 0; j < std::min(_l2,l2); j ++) {
                for(int i = 0; i < std::min(_l1,l1); i ++) {
                    newarray.at(i,j,k) = at(i,j,k);
                }
            }
        }
        std::swap(newarray._d,_d);
        std::swap(newarray._l1,_l1);
        std::swap(newarray._l2,_l2);
        std::swap(newarray._l3,_l3);
    }

    T& at(int i) { _check_array_access(*this, i); return _d[i]; }
    const T& at(int i) const { _check_array_access(*this, i); return _d[i]; }
    T& at(int i, int j, int k) { _check_array3_access(*this, i, j, k); return _d[k*(_l2*_l1)+j*_l1+i]; }
    const T& at(int i, int j, int k) const { _check_array3_access(*this, i, j, k); return _d[k*(_l2*_l1)+j*_l1+i]; }
    T& operator[](int i) { _check_array_access(*this, i); return _d[i]; }
    const T& operator[](int i) const { _check_array_access(*this, i); return _d[i]; }

    void assign(const T& v) { for(int i = 0; i < size(); i ++) _d[i] = v; }
    void assign(int l1, int l2, int l3, const T& v) { realloc(l1, l2, l3); assign(v); }
    void assign(const Cube<T>& v) { realloc(v.size1(),v.size2(),v.size3()); for(int i = 0; i < size(); i ++) _d[i] = v._d[i]; }

    int _l1, _l2, _l3;
    T* _d;
};

#endif
