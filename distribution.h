#ifndef DISTRIBUTION_H
#define DISTRIBUTION_H

#include "stdmath.h"
#include "vec.h"
#include "func.h"
#include "array.h"

template<typename R>
struct Distribution1D {
    Distribution1D() {};
    explicit Distribution1D(const std::vector<R>& func) { init(func); }
    explicit Distribution1D(const std::vector<R>&& func) { init(func); }
    Distribution1D(const Distribution1D &dist) = default;
    Distribution1D(const Distribution1D &&dist) noexcept : func(std::move(dist.func)), cdf(std::move(dist.cdf)), funcInt(dist.funcInt) {}
    Distribution1D& operator=(const Distribution1D &dist) = default;
    Distribution1D& operator=(Distribution1D&& dist) { using std::swap; swap(*this, dist); return *this; }
    void init(const std::vector<R>& func) { auto f = func; init(std::move(f)); }
    void init(const std::vector<R>&& func);
    int sample(R u, R *pdf, R *val = 0) const;
    R pdf(R u) const;
    R pdf(int u) const;
    std::vector<R> func;
    std::vector<R> cdf;
    R funcInt;
};

typedef Distribution1D<float> Distribution1Df;
typedef Distribution1D<double> Distribution1Dd;

template<typename R>
void Distribution1D<R>::init(const std::vector<R>&& function) {
    func = function;
    cdf.resize(func.size()+1);

    cdf[0] = 0;
    for (int i = 1; i < cdf.size(); ++i) {
        cdf[i] = cdf[i-1] + func[i-1] / func.size();
    }

    funcInt = cdf[func.size()];
    if (funcInt == 0.f) {
        for (int i = 1; i < cdf.size(); ++i) cdf[i] = R(i) / R(func.size());
    } else {
        for (int i = 1; i < cdf.size(); ++i) cdf[i] /= funcInt;
    }
}

template<typename R>
int Distribution1D<R>::sample(R u, R *pdf, R *val) const {
    if (u >= 1.0f) u -= 1.0f; //wrap around
    auto ptr = std::upper_bound(cdf.begin(), cdf.end(), u);
    int offset = max(0, int(ptr-cdf.begin()-1));
    error_if_not_va(offset < func.size(), "incorrect cdf sampling, u = %f", u);
    error_if_not(u >= cdf[offset] && u < cdf[offset+1], "incorrect cdf sampling");

    R du = (u - cdf[offset]) / (cdf[offset+1] - cdf[offset]);
    error_if_not(!isnan(du), "problem with du(nan)");

    if (pdf) *pdf = func[offset] / funcInt;
    if (val) *val = (offset + du) / func.size();
    return offset;
}

template<typename R>
R Distribution1D<R>::pdf(R u) const {
    int iu = clamp(int(u * func.size()), 0, func.size()-1);
    if (funcInt == 0.f) return 0.f;
    return func[iu] / funcInt;
}

template<typename R>
R Distribution1D<R>::pdf(int iu) const {
    if (funcInt == 0.f) return 0.f;
    return func[iu] / funcInt;
}



template<typename R, int L>
struct SDistribution1D {
    SDistribution1D() {};
    explicit SDistribution1D(const std::array<R, L>& func) { init(func); }
    explicit SDistribution1D(const SDistribution1D &dist);
    SDistribution1D& operator=(const SDistribution1D &dist);
    void init(const std::array<R, L>& func);
    int sample(R u, R *pdf, R *val = 0) const;
    R pdf(R u) const;
    R pdf(int u) const;
    std::array<R, L>    func;
    std::array<R, L+1>  cdf;
    R                   funcInt;
};

template<typename R, int L>
SDistribution1D<R,L>::SDistribution1D(const SDistribution1D &dist)
: func(dist.func), cdf(dist.cdf), funcInt(dist.funcInt){ }

template<typename R, int L>
SDistribution1D<R,L>& SDistribution1D<R,L>::operator=(const SDistribution1D &dist) {
    func = dist.func; cdf = dist.cdf; funcInt = dist.funcInt;
}

template<typename R, int L>
void SDistribution1D<R,L>::init(const std::array<R, L>& function) {
    func = function;
    cdf[0] = 0;
    for (int i = 1; i < cdf.size(); ++i) {
        cdf[i] = cdf[i-1] + func[i-1] / func.size();
    }
    funcInt = cdf[func.size()];
    if (funcInt == 0.f) {
        for (int i = 1; i < cdf.size(); ++i) cdf[i] = R(i) / R(func.size());
    } else {
        for (int i = 1; i < cdf.size(); ++i) cdf[i] /= funcInt;
    }
}

template<typename R, int L>
int SDistribution1D<R,L>::sample(R u, R *pdf, R *val) const {
    if (u >= 1.0f) u -= 1.0f; //wrap around
    auto ptr = std::upper_bound(cdf.begin(), cdf.end(), u);
    int offset = max(0, int(ptr-cdf.begin()-1));
    error_if_not_va(offset < func.size(), "incorrect cdf sampling, u = %f", u);
    error_if_not(u >= cdf[offset] && u < cdf[offset+1], "incorrect cdf sampling");

    R du = (u - cdf[offset]) / (cdf[offset+1] - cdf[offset]);
    error_if_not(!isnan(du), "problem with du(nan)");

    if (pdf) *pdf = func[offset] / funcInt;
    if (val) *val = (offset + du) / func.size();
    return offset;
}

template<typename R, int L>
R SDistribution1D<R,L>::pdf(R u) const {
    int iu = clamp(int(u * func.size()), 0, func.size()-1);
    if (funcInt == 0.f) return 0.f;
    return func[iu] / funcInt;
}

template<typename R, int L>
R SDistribution1D<R,L>::pdf(int iu) const {
    if (funcInt == 0.f) return 0.f;
    return func[iu] / funcInt;
}

template<typename R>
struct Distribution2D {
    Distribution2D() {};
    explicit Distribution2D(const Image<R>& func) { init(func); }
    Distribution2D(const Distribution2D &dist) = default;
    Distribution2D(const Distribution2D &&dist) noexcept;
    Distribution2D& operator=(const Distribution2D &dist) = default;
    Distribution2D& operator=(Distribution2D&& dist) noexcept { using std::swap; swap(*this, dist); return *this; }
    void init(const Image<R>& func);

    vec2<R> sample(const vec2<R>& s, R *pdf) const;
    R pdf(const vec2<R> &uv) const;

    std::vector<Distribution1D<R>>  conditionalV;
    Distribution1D<R>               marginal;
    R                               funcInt;
};

typedef Distribution2D<float> Distribution2Df;
typedef Distribution2D<double> Distribution2Dd;

template<typename R, int W, int H>
Distribution2D<R> makeDistribution2D(SImage<R, W, H> &func) {
    Distribution2D<R> dist;
    dist.conditionalV.resize(func.height());
    for (int v = 0; v < func.height(); v++) {
        std::vector<R> condBuffer(func.width());
        for(int u = 0; u < func.width(); u++)
            condBuffer.at(u) = func.at(u,v);
        dist.conditionalV[v].init(std::move(condBuffer));
    }
    // Compute marginal sampling distribution $p[\tilde{v}]$
    std::vector<R> marginalFunc(func.height());
    dist.funcInt = 0;
    for (int v = 0; v < func.height(); ++v) {
        marginalFunc[v] = dist.conditionalV[v].funcInt;
        dist.funcInt += dist.conditionalV[v].funcInt;
    }
    dist.marginal.init(std::move(marginalFunc));
    return dist;
}

template<typename R>
Distribution2D<R>::Distribution2D(const Distribution2D &&dist) noexcept :
conditionalV(std::move(dist.conditionalV)),
marginal(std::move(dist.marginal)),
funcInt(dist.funcInt) {}

template<typename R>
void Distribution2D<R>::init(const Image<R>& func) {
    conditionalV.resize(func.height());
    for (int v = 0; v < func.height(); v++) {
        std::vector<R> condBuffer(func.width());
        for(int u = 0; u < func.width(); u++)
            condBuffer.at(u) = func.at(u,v);
        conditionalV[v].init(std::move(condBuffer));
    }
    // Compute marginal sampling distribution $p[\tilde{v}]$
    std::vector<R> marginalFunc(func.height());
    funcInt = 0;
    for (int v = 0; v < func.height(); ++v) {
        marginalFunc[v] = conditionalV[v].funcInt;
        funcInt += conditionalV[v].funcInt;;
    }
    marginal.init(std::move(marginalFunc));
}

template<typename R>
vec2<R> Distribution2D<R>::sample(const vec2<R>& s, R *pdf) const {
    vec2f pdfs;
    vec2f uv;
    int v = marginal.sample(s.y, &pdfs.y, &uv.y);
    conditionalV[v].sample(s.x, &pdfs.x, &uv.x);
    *pdf = pdfs.x * pdfs.y;
    return uv;
}

template<typename R>
R Distribution2D<R>::pdf(const vec2<R> &uv) const {
    int iu = clamp<size_t>(uv.x * conditionalV[0].func.size(), 0,
                           conditionalV[0].func.size()-1);
    int iv = clamp<size_t>(uv.y * marginal.func.size(), 0,
                           marginal.func.size()-1);
    if (conditionalV[iv].funcInt * marginal.funcInt == 0.f) return 0.f;
    return (conditionalV[iv].func[iu] * marginal.func[iv]) /
    (conditionalV[iv].funcInt * marginal.funcInt);
}

#endif
