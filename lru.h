#ifndef _LRU_CACHE_H_
#define _LRU_CACHE_H_

//SINGLE THREADED LRU

#include <list>
#include <unordered_map>
#include <functional>

template<typename K>
struct lru {
    typedef std::list<K>                        lrulist;
    typedef typename lrulist::const_iterator    lrulistit;
    typedef std::unordered_map<K, lrulistit>    hashmap;
    typedef typename hashmap::iterator          hashmapit;
    int                                         _capacity;
    lrulist                                     _list;
    hashmap                                     _index;

    lru(int capacity = std::numeric_limits<int>::max()) {
        _capacity = capacity;
    }
    ~lru() { clear(); }

    inline bool touch(const K &key) {
        hashmapit it = _index.find( key );
        if( it == _index.end() ) {
            return false;
        }
        _list.splice(it->second, _list, _list.end());
        return true;
    }

    inline void resize(int capacity) {
        while(capacity < _list.size()) {
            evict();
        }
        _capacity = capacity;
    }

    inline void clear(void) {
        // unload;
        _list.clear();
        _index.clear();
    }

    inline K evict() {
        lrulistit liter = _list.begin();
        K key = *liter;
        _index.erase(*liter);
        _list.erase(liter);
        return key;
    }

    inline void insert(const K &key) {
        if(size() >= capacity()) evict();
        lrulistit it = _list.insert(_list.end(), key);
        _index[key] = it;
    }

    inline void remove(const K &key) {
        hashmapit it = _index.find( key );
        if( it == _index.end() ) {
            return;
        } else {
            _list.erase(it->second);
            _index.erase(it);
        }
    }

    inline bool contain(const K &key) const { return _index.find(key) != _index.end(); }

    inline int size() const { return _list.size(); }
    inline int capacity() const { return _capacity; }
    inline bool full() const { return size() == capacity(); }
};


template<typename K, typename V>
struct lru_cache {
    typedef std::list<std::pair<K, V>>          lrulist;
    typedef typename lrulist::iterator          lrulistit;
    typedef std::unordered_map<K, lrulistit>    hashmap;
    typedef typename hashmap::iterator          hashmapit;
    int                                         _capacity;
    lrulist                                     _list;
    hashmap                                     _index;

    std::function<V (K)>                        _loadfunc;
    std::function<void (V)>                     _evitfunc;

    lru_cache() : _capacity(0) {};
    lru_cache(std::function<V (K)> loadfunc, std::function<void (V)> evictfunc,
              int capacity = std::numeric_limits<int>::max()) {
        init(loadfunc, evictfunc, capacity);
    }
    ~lru_cache() { clear(); }

    void init(std::function<V (K)> loadfunc, std::function<void (V)> evicfunc,
              int capacity = std::numeric_limits<int>::max()) {
        _capacity = capacity;
        _loadfunc = loadfunc;
        _evitfunc = evicfunc;
    }

    inline V operator()(const K &key) {
        V value;
        fetch(key, value);
        return value;
    }

    bool fetch(const K &key, V &value) {
        typename hashmap::iterator it = _index.find(key);
        if(it == _index.end()) {
            if(size() >= capacity()) evict();
            value = _loadfunc(key);
            lrulistit it = _list.insert(_list.end(), {key, value});
            _index[key] = it;
            return false;
        } else {
            _list.splice(it->second, _list, _list.end());
            value = it->second->second;
            return true;
        }
    }

    bool try_fetch(const K &key, V &value) const {
        typename hashmap::const_iterator it = _index.find(key);
        if(it != _index.end()) {
            value = it->second->second;
            return true;
        } else {
            return false;
        }
    }

    inline bool touch(const K &key) {
        hashmapit it = _index.find( key );
        if( it == _index.end() ) {
            return false;
        }
        _list.splice(it->second, _list, _list.end());
        return true;
    }

    inline void resize(int capacity) {
        while(capacity < _list.size()) {
            evict();
        }
        _capacity = capacity;
    }

    inline void clear(void) {
        while(_list.size() > 0) {
            evict();
        }
    }

    inline K evict() {
        typename lrulist::iterator liter = _list.begin();
        _evitfunc(liter->second);
        _index.erase(liter->first);
        _list.erase(liter);
        return liter->first;
    }

    inline bool contain(const K &key) const { return _index.find(key) != _index.end(); }

    inline int size() const { return _list.size(); }
    inline int capacity() const { return _capacity; }
    inline bool full() const { return size() == capacity(); }
};

#endif //_LRU_CACHE_H_
