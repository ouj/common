#ifndef RANGE_H
#define RANGE_H

#include "stdmath.h"
#include "debug.h"
#include "vec.h"

template <typename R>
struct range1 {
    R min, max;
    range1() {}
    range1(const R min, const R max) : min(min), max(max) {}

    inline bool isvalid() const { return max >= min; }
    inline R    center() const { return (min + max) * 0.5f; }
    inline R    extent() const { error_if_not(isvalid(), "extent: invalid range"); return (max - min); }

    inline void grow(const range1& r) { min = std::min(min,r.min); max = std::max(max, r.max); }
    inline void grow(const R v) { min = std::min(min,v); max = std::max(max, v); }

    inline bool contain(const R v) const { return v >= min && v <= max; }
    inline bool contain(const range1 &v) const { return contain(v.min) && contain(v.max); }
    inline R    length() const { return max - min; }
    inline std::string print() const { char s[64]; sprintf(s, "<%f,%f>", min, max); return std::string(s); }

    friend bool operator==(const range1& a, const range1& b) { return a.min == b.min && a.max == b.max; }
    friend bool operator!=(const range1& a, const range1& b) { return !(a==b); }
};

template <typename R>
inline range1<R> invalidrange1() {
    return range1<R>(consts<R>::max, -consts<R>::max);
}

template <typename R>
struct range2 {
    vec2<R> min, max;
    range2() {}
    range2(const vec2<R> &min, const vec2<R>& max) : min(min), max(max) {}

    inline bool     isvalid() const { return max.x >= min.x && max.y >= min.y; }
    inline vec2<R>  center() const { return vec2<R>((min.x + max.x) * 0.5f, (min.y + max.y) * 0.5f); }
    inline vec2<R>  extent() const { error_if_not(isvalid(), "extent: invalid range"); return (max - min); }
    inline R        diagonal() const { return length(extent()); }
    inline R        diagonalSqr() const { return lengthSqr(extent()); }

    inline void     grow(const range2& r) { min = min_component(min,r.min); max = max_component(max, r.max); }
    inline void     grow(const vec2<R> &v) { min = min_component(min,v); max = max_component(max, v); }

    inline bool     contain(const vec2<R> &v) const { return v.x >= min.x && v.y >= min.y &&
                                                         v.x <= max.x && v.y <= max.y; }
    inline bool     contain(const range2 &v) const { return contain(v.min) && contain(v.max); }
    inline R        area() const { return (max.x - min.x) * (max.y - min.y); }
    std::string     print() const;

    friend bool operator==(const range2& a, const range2& b) { return a.min == b.min && a.max == b.max; }
    friend bool operator!=(const range2& a, const range2& b) { return !(a==b); }
};

template<typename R>
std::string range2<R>::print() const {
    char s[64]; sprintf(s, "<%s,%s>", min.print().c_str(), max.print().c_str()); return std::string(s);
}

template<typename R>
inline range2<R> runion(const range2<R>& range, const range2<R>& r) {
    return range2<R>(min_component(range.min,r.min),max_component(range.max,r.max));
}

template<typename R>
inline range2<R> intersect(const range2<R>& r1, const range2<R> &r2) {
    return range2<R>(max_component(r1.min, r2.min),
                     min_component(r1.max, r2.max));
}

template <typename R>
inline range2<R> invalidrange2() {
    return range2<R>(vec2<R>(consts<R>::max, consts<R>::max),
                     vec2<R>(-consts<R>::max, -consts<R>::max));
}

template <typename R>
struct range3 {
    vec3<R> min, max; // min and max corners
    range3() {}
    range3(const vec3<R> &min, const vec3<R>& max) : min(min), max(max) {}

    inline bool     isvalid() const { return max.x >= min.x && max.y >= min.y && max.z >= min.z; }
    inline vec3<R>  center() const { return (min + max) * 0.5f; }
    inline vec3<R>  extent() const { return (max - min); }
    inline R        diagonal() const { return extent().length(); }
    inline R        diagonalSqr() const { return extent().lengthSqr(); }

    inline void     grow(const range3& r) { min = min_component(min,r.min); max = max_component(max, r.max); }
    inline void     grow(const vec3<R> &v) { min = min_component(min,v); max = max_component(max, v); }

    inline bool     contain(const vec3<R> &v) const { return v.x >= min.x && v.y >= min.y && v.z >= min.z &&
                                                         v.x <= max.x && v.y <= max.y && v.z <= max.z; }
    inline bool     contain(const range3 &v) const { return contain(v.min) && contain(v.max); }
    inline R        volume() const { return (max.x - min.x) * (max.y - min.y) * (max.z - min.z); }
    std::string     print() const;

    friend bool operator==(const range3& a, const range3& b) { return a.min == b.min && a.max == b.max; }
    friend bool operator!=(const range3& a, const range3& b) { return !(a==b); }
};

template<typename R>
inline std::string range3<R>::print() const {
    char s[64]; sprintf(s, "<%s,%s>", min.print().c_str(), max.print().c_str()); return std::string(s);
}

template<typename R>
inline range3<R> runion(const range3<R>& range, const range3<R>& r) {
    return range3<R>(min_component(range.min,r.min),max_component(range.max,r.max));
}

template<typename R>
inline range3<R> intersect(const range3<R>& r1, const range3<R> &r2) {
    return range3<R>(max_component(r1.min, r2.min),
                     min_component(r1.max, r2.max));
}

template<typename R>
inline bool overlap(const range3<R>& r1, const range3<R> &r2) {
    return intersect(r1, r2).isvalid();
}

template <typename R>
inline range3<R> invalidrange3() {
    return range3<R>(makevec3<R>(numeric_limits<R>::max()),
                     makevec3<R>(-numeric_limits<R>::max()));
}

template<typename R>
inline range3<R> scale(const range3<R>& range, R s) {
    error(isvalid(range), "scale: invalid range");
    vec3<R> rc = center(range);
    vec3<R> rs = size(range);
    return range3<R>(rc-rs*((R)0.5*s), rc+rs*((R)0.5*s));
}

typedef range1<int>             range1i;
typedef range1<unsigned int>    range1u;
typedef range1<float>           range1f;
typedef range1<double>          range1d;

typedef range2<int>             range2i;
typedef range2<unsigned int>    range2u;
typedef range2<float>           range2f;
typedef range2<double>          range2d;

typedef range3<int>             range3i;
typedef range3<unsigned int>    range3u;
typedef range3<float>           range3f;
typedef range3<double>          range3d;

const range3d invalidrange3d = invalidrange3<double>();
const range2d invalidrange2d = invalidrange2<double>();
const range1d invalidrange1d = invalidrange1<double>();
const range3f invalidrange3f = invalidrange3<float>();
const range2f invalidrange2f = invalidrange2<float>();
const range1f invalidrange1f = invalidrange1<float>();
const range3i invalidrange3i = invalidrange3<int>();
const range2i invalidrange2i = invalidrange2<int>();
const range1i invalidrange1i = invalidrange1<int>();
const range3u invalidrange3u = invalidrange3<unsigned int>();
const range2u invalidrange2u = invalidrange2<unsigned int>();
const range1u invalidrange1u = invalidrange1<unsigned int>();

#endif
